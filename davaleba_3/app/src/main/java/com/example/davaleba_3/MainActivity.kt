package com.example.davaleba_3

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {

    val items = ArrayList<ItemModel>()
    private lateinit var adapter: RecyclerViewAdapter
    private val REQUEST_CODE = 123

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init(){
        addItemButton.setOnClickListener {
            addItem(items)
        }

        setData()
        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = RecyclerViewAdapter(items, this)
        recyclerView.adapter = adapter
    }

    private fun setData(){

        val date: String = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(Date())

        items.add(ItemModel(R.mipmap.ic_launcher, "title1", "description1", date))
        items.add(ItemModel(R.mipmap.ic_launcher, "title2", "description2", date))
        items.add(ItemModel(R.mipmap.ic_launcher, "title3", "description3", date))
        items.add(ItemModel(R.mipmap.ic_launcher, "title4", "description4", date))
        items.add(ItemModel(R.mipmap.ic_launcher, "title5", "description5", date))
        items.add(ItemModel(R.mipmap.ic_launcher, "title6", "description6", date))
    }

    private fun addItem(list: ArrayList<ItemModel>){
        val intent = Intent(this, ItemActivity::class.java)
        startActivityForResult(intent, REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE){
            val title = data!!.extras!!.getString("title", "")
            val description = data.extras!!.getString("description", "")

            val date: String = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(Date())
            items.add(0,ItemModel(R.mipmap.ic_launcher, title , description, date))
            adapter.notifyItemInserted(0)
            recyclerView.scrollToPosition(0)
        }

        super.onActivityResult(requestCode, resultCode, data)
    }
}
