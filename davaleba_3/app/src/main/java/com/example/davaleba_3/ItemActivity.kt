package com.example.davaleba_3

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_item.*

class ItemActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item)
        init()
    }

    private fun init(){
        saveItemButton.setOnClickListener {
            saveItem()
        }
    }

    private fun saveItem(){
        val intent = intent
        intent.putExtra("title", titleEditText.text.toString())
        intent.putExtra("description", descriptionEditText.text.toString())

        setResult(Activity.RESULT_OK, intent)
        finish()
    }
}
