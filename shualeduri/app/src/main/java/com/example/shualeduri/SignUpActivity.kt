package com.example.shualeduri

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        auth = FirebaseAuth.getInstance()

        signUpButton.setOnClickListener {
            signUpUser()
        }
    }

    private fun signUpUser(){
        if(userNameEditText.text.toString().isEmpty()){
            userNameEditText.error = "Enter username"
            return
        }

        if(!Patterns.EMAIL_ADDRESS.matcher(userNameEditText.text.toString()).matches()){
            userNameEditText.error = "Enter valid email"
            userNameEditText.requestFocus()
            return
        }

        if(passwordEditText.text.toString().isEmpty()){
            passwordEditText.error = "Enter password"
            passwordEditText.requestFocus()
            return
        }

        auth.createUserWithEmailAndPassword(userNameEditText.text.toString(), passwordEditText.text.toString())
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    val user = auth.currentUser
                    user!!.sendEmailVerification()
                        .addOnCompleteListener { task ->
                            if (task.isSuccessful) {
                                val intent = Intent(this, MainActivity::class.java)
                                startActivity(intent)
                                finish()
                            }
                        }
                } else {
                    Toast.makeText(baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
                }
            }
    }
}
