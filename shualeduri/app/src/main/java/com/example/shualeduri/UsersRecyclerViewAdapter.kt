package com.example.davaleba_4

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.shualeduri.R
import com.example.shualeduri.UserActivity
import com.example.shualeduri.UsersModel
import kotlinx.android.synthetic.main.user_recyclerview_layout.view.*

class UsersRecyclerViewAdapter(private val users: ArrayList<UsersModel.Data>, private val activity: UserActivity) :
    RecyclerView.Adapter<UsersRecyclerViewAdapter.ViewHolder>() {

    override fun getItemCount() = users.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.user_recyclerview_layout,parent,false))
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        private lateinit var model: UsersModel.Data

        fun onBind(){
            model = users[adapterPosition]
            Glide.with(activity).load(model.avatar).into(itemView.imageView)
            itemView.fullNameTextView.text = "${model.firstName} ${model.lastName}"
            itemView.emailTextView.text = model.email
        }
    }
}