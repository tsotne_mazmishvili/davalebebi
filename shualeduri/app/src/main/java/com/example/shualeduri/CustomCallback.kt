package com.example.shualeduri

interface CustomCallback {
    fun onSuccess(result: String) {}
    fun onFailure(errMessage: String) {}
}