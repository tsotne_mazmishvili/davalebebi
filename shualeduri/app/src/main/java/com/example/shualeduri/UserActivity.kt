package com.example.shualeduri

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.davaleba_4.UsersRecyclerViewAdapter
import com.google.firebase.auth.FirebaseAuth
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_user.*


class UserActivity : AppCompatActivity() {

    private lateinit var adapter: UsersRecyclerViewAdapter
    private var users = ArrayList<UsersModel.Data>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)
        logOut()
        getusers()
    }

    fun logOut(){
        logoutButton.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun getusers(){
        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = UsersRecyclerViewAdapter(users, this)
        recyclerView.adapter = adapter

        DataLoader.getRequest("2", object :CustomCallback{
            override fun onSuccess(result: String) {
                val model  = Gson().fromJson(result, UsersModel::class.java)
                    users.addAll(model.data)
                    adapter.notifyDataSetChanged()
            }
        })
    }
}
