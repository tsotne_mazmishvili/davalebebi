package com.example.shualeduri

import android.util.Log
import android.util.Log.d
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.*

object DataLoader {
    var retrofit = Retrofit.Builder()
        .addConverterFactory(ScalarsConverterFactory.create())
        .baseUrl("https://reqres.in/api/")
        .build()

    var service = retrofit.create(ApiRetrofit::class.java)

    fun getRequest(page: String, customCallback: CustomCallback){
        val call = service.getRequest(page)
        call.enqueue(callback(customCallback))
    }

    fun postRequest(path: String, parameters:MutableMap<String, String>, customCallback: CustomCallback){
        val call = service.postRequest(path, parameters)
        call.enqueue(callback(customCallback))
    }
    private fun callback(customCallback: CustomCallback) = object : Callback<String> {
        override fun onFailure(call: Call<String>, t: Throwable) {
            Log.d("getRequestt", "${t.message}")
            customCallback.onFailure(t.message.toString())
        }

        override fun onResponse(call: Call<String>, response: Response<String>) {
            Log.d("getRequest", "${response.body()}")
            customCallback.onSuccess(response.body().toString())
        }
    }
}

interface ApiRetrofit{
    @GET("users")
    fun getRequest(@Query("page")page: String): Call<String>

    @FormUrlEncoded
    @POST("{path}")
    fun postRequest(@Path("path")path: String, parameters:Map<String, String>): Call<String>
}