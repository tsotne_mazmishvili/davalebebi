package com.example.davaleba_2

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val REQUEST_CODE = 123

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        setStaticVariables()
    }

    private fun init(){
        editProfile.setOnClickListener {
            openSecondActivity()
        }
    }

    private fun openSecondActivity(){
        var userModel = UserModel(nameTextView.text.toString(), surnameTextView.text.toString(), emailTextView.text.toString(),
        birthDateTextView.text.toString().toInt(), genderTextView.text.toString())
        val intent = Intent(this, SecondActivity::class.java)
        intent.putExtra("userModel", userModel)
        startActivityForResult(intent, REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE){
            val name = data!!.extras!!.getString("name", "")
            val surname = data!!.extras!!.getString("surname", "")
            val email = data!!.extras!!.getString("email", "")
            val birthDate = data!!.extras!!.getString("birthDate", "")
            val gender = data!!.extras!!.getString("gender", "")

            nameTextView.text = name
            surnameTextView.text = surname
            emailTextView.text = email
            birthDateTextView.text = birthDate
            genderTextView.text = gender
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun setStaticVariables(){
        //რათქმაუნდა activity.main.xml-შიც შეიძლებოდა დასეტვა სტატიკურად მაგრამ ასე ვარჩიე
        nameTextView.text = "Tsotne"
        surnameTextView.text = "Mazmishvili"
        emailTextView.text = "tsotne.mazmishvili.1@btu.edu.ge"
        birthDateTextView.text = 1999.toString()
        genderTextView.text = "Male"
    }
}
