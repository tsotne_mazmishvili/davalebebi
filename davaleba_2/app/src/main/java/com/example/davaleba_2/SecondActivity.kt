package com.example.davaleba_2

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        init()
    }

    private fun init(){
        saveProfileButton.setOnClickListener {
            saveProfile()
        }
    }

    private fun saveProfile(){
        val intent = intent
        intent.putExtra("name", nameEditText.text.toString())
        intent.putExtra("surname", surnameEditText.text.toString())
        intent.putExtra("email", emailEditText.text.toString())
        intent.putExtra("birthDate", birthDateEditText.text.toString())
        intent.putExtra("gender", genderEditText.text.toString())

        setResult(Activity.RESULT_OK, intent)
        finish()
    }
}
